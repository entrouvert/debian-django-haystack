Source: django-haystack
Section: python
Priority: optional
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Michael Fladischer <fladi@debian.org>
Build-Depends: debhelper (>= 9),
               dh-python,
               python-all,
               python-setuptools,
               python-sphinx (>= 1.0.7+dfsg),
               python3-all,
               python3-setuptools
Standards-Version: 3.9.6
X-Python-Version: >= 2.6
X-Python3-Version: >= 3.3
Homepage: https://github.com/toastdriven/django-haystack
Vcs-Svn: svn://anonscm.debian.org/python-modules/packages/django-haystack/trunk/
Vcs-Browser: http://anonscm.debian.org/viewvc/python-modules/packages/django-haystack/trunk/

Package: python-django-haystack
Architecture: all
Depends: python-django (>= 1.5),
         ${misc:Depends},
         ${python:Depends}
Suggests: python-elasticsearch,
          python-httplib2,
          python-pysolr (>= 3.2.0),
          python-whoosh
Description: modular search for Django
 Haystack provides modular search for Django. It features a unified, familiar
 API that allows you to plug in different search backends (such as Solr,
 Elasticsearch, Whoosh, Xapian, etc.) without having to modify your code.
 .
 It plays nicely with third-party app without needing to modify the source and
 supports advanced features like faceting, More Like This, highlighting, spatial
 search and spelling suggestions.

Package: python3-django-haystack
Architecture: all
Depends: python3-django,
         ${misc:Depends},
         ${python3:Depends}
Suggests: python3-elasticsearch,
          python3-httplib2,
          python3-whoosh
Description: modular search for Django (Python3 version)
 Haystack provides modular search for Django. It features a unified, familiar
 API that allows you to plug in different search backends (such as Solr,
 Elasticsearch, Whoosh, Xapian, etc.) without having to modify your code.
 .
 It plays nicely with third-party app without needing to modify the source and
 supports advanced features like faceting, More Like This, highlighting, spatial
 search and spelling suggestions.
 .
 This package contains the Python 3 version of the library.

Package: python-django-haystack-doc
Section: doc
Architecture: all
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: modular search for Django (Documentation)
 Haystack provides modular search for Django. It features a unified, familiar
 API that allows you to plug in different search backends (such as Solr,
 Elasticsearch, Whoosh, Xapian, etc.) without having to modify your code.
 .
 It plays nicely with third-party app without needing to modify the source and
 supports advanced features like faceting, More Like This, highlighting, spatial
 search and spelling suggestions.
 .
 This package contains the documentation.
