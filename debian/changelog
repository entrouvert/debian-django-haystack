django-haystack (2.4.0-2) unstable; urgency=low

  * Switch buildsystem to pybuild.
  * Add Python3 support through a separate package.
  * Add lintian override for missing upstream changelog.

 -- Michael Fladischer <fladi@debian.org>  Tue, 07 Jul 2015 22:12:20 +0200

django-haystack (2.4.0-1) unstable; urgency=low

  * New upstream release.
  * Remove files from d/copyright which are no longer shipped by
    upstream.
  * Use pypi.debian.net service for uscan.
  * Change my email address to fladi@debian.org.

 -- Michael Fladischer <fladi@debian.org>  Tue, 07 Jul 2015 16:18:03 +0200

django-haystack (2.3.1-1) unstable; urgency=medium

  * New upstream release (Closes: #755599).
  * Bump Standards-Version to 3.9.6.
  * Disable tests as they require a live SOLR and elasticsearch server.
  * Change file names for solr configuration files in d/copyright.
  * Make pysolr require at least version 3.2.0.
  * Add python-elasticsearch to Suggests.
  * Drop packages required by tests from Build-Depends:
    + python-django
    + python-httplib2
    + python-mock
    + python-pysolr
    + python-whoosh
  * Drop python-xapian from suggests as the xapian backend is not
    included.
  * Add django_haystack.egg-info/requires.txt to d/clean.
  * Remove empty lines at EOF for d/clean and d/rules.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Mon, 20 Oct 2014 14:18:24 +0200

django-haystack (2.1.0-1) unstable; urgency=low

  * Initial release (Closes: #563311).

 -- Michael Fladischer <FladischerMichael@fladi.at>  Thu, 13 Mar 2014 19:11:15 +0100
